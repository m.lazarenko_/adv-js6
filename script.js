
// Асинхронність у js - це коли щось спрацьовує не одночасно, а чекає виконання операції, тобто умовний ланцюжок виконання.




const btn = document.querySelector(".btn")

async function getLocation(){
    
    let url = "https://api.ipify.org/?format=json";

    let response = await fetch(url);

    let data = await response.json();


    let locationUrl = `http://ip-api.com/json/${data.ip}?fields=continent,country,regionName,city,district`;

    let responseL = await fetch(locationUrl);

    let result = await responseL.json();
    
    console.log(result);

    let {continent, country, regionName, city, district } = result;
    let list = document.createElement("ul");
       list.innerHTML = `
            <li>Continent: ${continent}</li>
            <li>Country: ${country}</li>
            <li>Region: ${regionName}</li>
            <li>City: ${city}</li>
            <li>District: ${district}</li>          
       ` 
       document.body.append(list) 
    
}




btn.addEventListener('click', getLocation)
